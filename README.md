# ms-2020-docker

## 1. Install Docker

Go to https://www.docker.com/products/docker-desktop and download Docker for your OS.

You may need tp restart your computer to allow Docker to run correctly.

Check your version of Docker by starting the Terminal and running this command:

`docker version`

## Test Docker installation:

```
 $ docker run hello-world

    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    ca4f61b1923c: Pull complete
    Digest: sha256:ca0eeb6fb05351dfc8759c20733c91def84cb8007aa89a5bf606bc8b315b9fc7
    Status: Downloaded newer image for hello-world:latest

    Hello from Docker!
    This message shows that your installation appears to be working correctly.
    ...
```

Run `docker image ls` to list the hello-world image that you downloaded to your machine.
    

List the `hello-world` container (spawned by the image) which exits after displaying its message. If it is still running, you do not need the `--all` option:

```
$ docker container ls --all

    CONTAINER ID     IMAGE           COMMAND      CREATED            STATUS
    54f4984ed6a8     hello-world     "/hello"     20 seconds ago     Exited (0) 19 seconds ago
```

## Build and test your image

Clone this example repository:

```
git clone https://github.com/dockersamples/node-bulletin-board
cd node-bulletin-board/bulletin-board-app
```

The `node-bulletin-board` project is a simple bulletin board application, written in Node.js. In this example, let’s imagine you wrote this app, and are now trying to containerize it.

Take a look at the file called `Dockerfile` in the bulletin board application. Dockerfiles describe how to assemble a private filesystem for a container, and can also contain some metadata describing how to run a container based on this image. The bulletin board app Dockerfile looks like this:

```Docker
# Use the official image as a parent image
FROM node:current-slim

# Set the working directory
WORKDIR /usr/src/app

# Copy the file from your host to your current location
COPY package.json .

# Run the command inside your image filesystem
RUN npm install

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 8080

# Run the specified command within the container.
CMD [ "npm", "start" ]

# Copy the rest of your app's source code from your host to your image filesystem.
COPY . .
```

Writing a Dockerfile is the first step to containerizing an application. You can think of these Dockerfile commands as a step-by-step recipe on how to build up your image. This one takes the following steps:

    
*  Start `FROM` the pre-existing `node:current-slim` image. This is an official image, built by the node.js vendors and validated by Docker to be a high-quality image containing the Node.js Long Term Support (LTS) interpreter and basic dependencies.
*  Use `WORKDIR` to specify that all subsequent actions should be taken from the directory `/usr/src/app` in your image filesystem (never the host’s filesystem).
*  `COPY` the file package.json from your host to the present location (.) in your image (so in this case, to `/usr/src/app/package.json`)
*  `RUN` the command npm install inside your image filesystem (which will read `package.json` to determine your app’s node dependencies, and install them)
*  `COPY` in the rest of your app’s source code from your host to your image filesystem.

You can see that these are much the same steps you might have taken to set up and install your app on your host. However, capturing these as a Dockerfile allows you to do the same thing inside a portable, isolated Docker image.

The steps above built up the filesystem of our image, but there are other lines in your Dockerfile.

The `CMD` directive is the first example of specifying some metadata in your image that describes how to run a container based on this image. In this case, it’s saying that the containerized process that this image is meant to support is npm start.

The `EXPOSE 8080` informs Docker that the container is listening on port 8080 at runtime.

What you see above is a good way to organize a simple Dockerfile; always start with a FROM command, follow it with the steps to build up your private filesystem, and conclude with any metadata specifications. There are many more Dockerfile directives than just the few you see above. For a complete list, see the Dockerfile reference.

## Build and test your image

Now that you have some source code and a Dockerfile, it’s time to build your first image, and make sure the containers launched from it work as expected.

Make sure you’re in the directory `node-bulletin-board/bulletin-board-app` in a terminal or PowerShell using the `cd` command. Let’s build your bulletin board image:

```
docker image build -t bulletinboard:1.0 .
```

You’ll see Docker step through each instruction in your Dockerfile, building up your image as it goes. If successful, the build process should end with a message 

`Successfully tagged bulletinboard:1.0.`

Start a container based on your new image:

```
docker container run --publish 8000:8080 --detach --name bb bulletinboard:1.0
```

There are a couple of common flags here:

* publish asks Docker to forward traffic incoming on the host’s port 8000, to the container’s port 8080. Containers have their own private set of ports, so if you want to reach one from the network, you have to forward traffic to it in this way. Otherwise, firewall rules will prevent all network traffic from reaching your container, as a default security posture.
* detach asks Docker to run this container in the background.
* name specifies a name with which you can refer to your container in subsequent commands, in this case bb.
    
Also notice, you didn’t specify what process you wanted your container to run. You didn’t have to, as you’ve used the CMD directive when building your Dockerfile; thanks to this, Docker knows to automatically run the process npm start inside the container when it starts up.

Visit your application in a browser at `localhost:8000`. You should see your bulletin board application up and running. At this step, you would normally do everything you could to ensure your container works the way you expected; now would be the time to run unit tests, for example.



Once you’re satisfied that your bulletin board container works correctly, you can delete it:
```
docker container rm --force bb
```
The --force option removes the running container.

## Conclusion

At this point, you’ve successfully built an image, performed a simple containerization of an application, and confirmed that your app runs successfully in its container. The next step will be to share your images on [Docker Hub](https://hub.docker.com/), so they can be easily downloaded and run on any destination machine.

## Share your image

### Set up your Docker Hub account

If you don’t yet have a Docker ID, follow these steps to set one up; this will allow you to share images on Docker Hub.

1. Visit the Docker Hub sign up page, https://hub.docker.com/signup.
2. Fill out the form and submit to create your Docker ID.
2. Verify your email address to complete the registration process.
3. Click on the Docker icon in your toolbar or system tray, and click Sign in / Create Docker ID.
4. Fill in your new Docker ID and password. After you have successfully authenticated, your Docker ID appears in the Docker Desktop menu in place of the ‘Sign in’ option you just used.

You can do the same thing from the command line by typing 

```bash
docker login
```

## Create a Docker Hub repository and push your image

At this point, you’ve set up your Docker Hub account and have connected it to your Docker Desktop. Now let’s make our first repo, and share our bulletin board app there.

1.  Click on the Docker icon in your menu bar, and navigate to Repositories > Create. You’ll be taken to a Docker Hub page to create a new repository.
2.  Fill out the repository name as bulletinboard. Leave all the other options alone for now, and click Create at the bottom.
3.  Now you are ready to share your image on Docker Hub, but there’s one thing you must do first: images must be namespaced correctly to share on Docker Hub. Specifically, you must name images like <Docker ID>/<Repository Name>:<tag>. You can relabel your bulletinboard:1.0 image like this (of course, please replace gordon with your Docker ID):

```bash
docker image tag bulletinboard:1.0 gordon/bulletinboard:1.0
```

4. Finally, push your image to Docker Hub:
```bash
docker image push gordon/bulletinboard:1.0
```

Visit your repository in Docker Hub, and you’ll see your new image there. 

## Try to find an interesting Docker Images on Docker Hub and build and run them by yourself

[Wordpress](https://hub.docker.com/_/wordpress)
[NGINX](https://hub.docker.com/_/nginx)
[httpd](https://hub.docker.com/_/httpd)